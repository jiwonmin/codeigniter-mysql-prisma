// ------------------------------
// ----------- prisma -----------
const { PrismaClient } = require('@prisma/client');
const { ApolloServer } = require('apollo-server');
const prisma = new PrismaClient();
const path = require('path');
const fs = require('fs');
const Query = require('./resolvers/Query');
const Mutation = require('./resolvers/Mutation');
const resolvers = {
    Query,
    Mutation,
};
const server = new ApolloServer({
    typeDefs: fs.readFileSync(
        path.join(__dirname, 'schema.graphql'),
        'utf8'
    ),
    resolvers,
    context: {
        prisma,
    }
});

server.listen().then(({ url }) => console.log(`Server is running on ${url}`));


// -------------------------------
// ---------- socket.io ----------
var express = require('express');
var cors = require('cors');
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"]
  }
});
var public_channel = "channel_public";
var sessions = [];

app.use('/chat', express.static(__dirname + '/chat'));
app.use(cors()); 

app.get('/', function(req, res) {
  res.sendFile(__dirname + '/chat/index.html');
});

io.on('connection', function (socket) {
  sessions.push(socket.handshake.query.loginID);
  sessions = [...new Set(sessions)];

  socket.emit("session_check", sessions);

  socket.on(public_channel, function(message) {
    if (message.to && message.from) {
      io.emit("channel_" + message.to, message);
    }
  });

  // socket.on('disconnect', () => {
  //   sessions.forEach((o, i) => {
  //     console.log(o.socketID);
  //     if (o.socketID === socket.id) {
  //       delete sessions[i];
  //     }
  //   });
  
  //   console.log(sessions);
  
  //   sessions = sessions.filter((el) => {return el != null;});
  //   io.emit("session_check", sessions);
  // });
});

http.listen(3000);

