function getLink(parent, args, context, info) {
    let findLink = [];
    if (args.id) {
        findLink.push(context.prisma.link.findUnique({
            where: {
                id: parseInt(args.id)
            }
        }));
    } else {
        findLink = context.prisma.link.findMany();
    }

    return findLink;
}

function getUser(parent, args, context) {
    let findUser = [];
    if (args.user_id) {
        findUser.push(context.prisma.users.findUnique({
            where: {
                user_id: args.user_id
            }
        }));
    } else {
        findUser = context.prisma.users.findMany();
    }

    return findUser;
}

function getBoard(parent, args, context) {
    let findBoard = [];
    if (args.idx) {
        findBoard.push(context.prisma.boards.findUnique({
            where: {
                idx: parseInt(args.idx)
            }
        }));
    } else {
        findBoard = context.prisma.boards.findMany();
    }

    return findBoard;
}

function login(parent, args, context) {
    var crypto = require('crypto');
    var password;

    if (args.user_id && args.password) {
        args.password = Buffer.from(crypto.createHash('sha512').update(args.password).digest('binary'), 'binary').toString('base64');
        return context.prisma.users.findMany({
            where: {
                AND: {
                    user_id: args.user_id,
                    password: args.password
                }
            }
        })
    }
}

module.exports = {
    getLink,
    getUser,
    getBoard,
    login
}