async function post(parent, args, context, info) {
    return await context.prisma.link.create({
        data: {
            url: args.url,
            description: args.description,
        },
    })
  }
  
  module.exports = {
    post,
  }