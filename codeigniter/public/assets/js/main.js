$(function() {
    $('.dropdown').dropdown({
        transition: 'drop'
    });

    let footerIcon = $('#chat .circular.icon');
    let chatRoom = $("#chat-room");
    footerIcon.transition('set looping').transition('bounce', '2000ms');
    
    footerIcon.on('mouseover', function(e) {
        footerIcon.transition('remove looping');
    }).on('mouseleave', function(e) {
        footerIcon.transition('set looping').transition('bounce', '2000ms');
    }).on('click', function(e) {
        
        footerIcon.unbind('mouseover, mouseleave');
        footerIcon.transition('scale');

        chatRoom.transition('scale');
    });

    $('.close-button').on('click', function() {
        chatRoom.transition('scale');
        footerIcon.transition('scale', function() {
            footerIcon.transition('set looping').transition('bounce', '2000ms'); 
        });
    });
});

function updateBoard(_this) {
    if ($(_this).attr('type') === 'button') {
        event.preventDefault();

        $('input[name=subject], textarea[name=content]').removeAttr('disabled');
        $('input[name=subject], textarea[name=content]').removeAttr('readonly');
        
        $(_this).attr('type', 'submit');
        $('input[name=_method]').val('put');

        $('form').attr('action', '/board/' + $('input[name=idx]').val());
    }
}

function deleteBoard(_this) {
    if ($(_this).attr('type') === 'button' && confirm('삭제하시겠습니까?')) {
        $(_this).attr('type', 'submit');
        $('input[name=_method]').val('delete');
        $('form').attr('action', '/board/' + $('input[name=idx]').val());
    }
}

function loginSubmit(form) {
    event.preventDefault();

    $.ajax({
        method:'post',
        url: '/api/user/login',
        data: {
            _method: form._method.value,
            user_id: form.id.value,
            password: form.pw.value
        },
        success: function(response) {
            var resp = JSON.parse(response);
            if (resp['result'] === 'success') {
                alert(resp['message']);
                location.href = resp['url'];
            } else {
                alert(resp['message']);
                console.log(resp['debug']);
            }
        }
    });
}