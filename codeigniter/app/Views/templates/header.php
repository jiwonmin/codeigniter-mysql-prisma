<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Boards</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css"/>
    <link rel="stylesheet" href="/assets/css/main.css"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/4.0.1/socket.io.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="/assets/js/main.js"></script>
</head>
<body>

<div id="wrapper">

    <div id="header">
        <div class="title"><?= esc($data['header-title']) ?></div>


        <div class="ui floating dropdown icon button" style="margin: 10px 0;">
            <i class="bars icon"></i>
            <div class="menu">
                <div class="item" data-value="home" onclick="location.href='/'">Home</div>
                <div class="item" data-value="boards" onclick="location.href='/boards'">Boards</div>

                <?php if (!$_SESSION['IS_LOGIN']) { ?>
                    <div class="item" data-value="login" onclick="location.href='/login'">Login</div>
                    <div class="item" data-value="join" onclick="location.href='/join'">Join</div>
                <?php } else { ?>
                    <div class="item" data-value="logout" onclick="location.href='/logout'">Logout</div>
                <?php } ?>
            </div>
        </div>
    </div>