<div id="chat-room" style="display:none;">
    <div class="ui card">
        <div class="content">
            <div class="header" style="display:flex; justify-content:space-between;align-items: center;">
                Customer Service
                <button class="circular ui icon button close-button"><i class="close icon"></i></button>
            </div>
        </div>
        <div class="content" style="height:25em;">
            <h4 id="title" class="ui sub header">Activity</h4>
            <ul id="messages" class="ui small feed">
                <li></li>
            </ul>
        </div>
        <div class="extra content">
            <form id="messaging" action="" class="ui form">
                <div class="ui action input">
                    <input type="text" id="m" autocomplete="off" style="width:210px;">
                    <button class="ui button">Send</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="chat">
    <button class="circular ui icon button">
        <i class="comments icon"></i>
    </button>
</div>

<script>
    (() => {
        var login = "<?= $_SESSION['LOGIN_ID'] ?>";
        var socket = io.connect('<?= "{$_SERVER['REQUEST_SCHEME']}://{$_SERVER['HTTP_HOST']}" ?>:3000', {query: `loginID=${login}`});
        var manager_login = false;

        socket.on("session_check", (sessions) => {
            console.log(sessions);
            if (sessions.includes("cs_manager")) {
                manager_login = true;
            }
        });

        if (manager_login) { 
            $("#title").text("CS Manager not login");
            return;
        }

        if (!login) {
            $("#title").text("Reload page with login parameter");
            return;
        } else {
            $("#title").text("You are logged in as " + login);
        }

        var public_channel = "channel_public";
        var private_channel = "channel_" + login;

        ///////////////////////////////////////////////////
        /// 메세지 발신
        $("form#messaging").submit(function() {
            var text_message = $('#m').val();
            var recipient = getRecipient(text_message);

            if (recipient != null && recipient.length > 0) {
                var message = {
                    to      : recipient,
                    from    : login,
                    message : text_message,
                    type    : 'text_message',
                }
                sendMessage(message);

                $('#messages').append($('<li>').text("You" + ": " + text_message));
                $('#m').val('').trigger('input');
            }

            return false;
        });

        var previous_typing_recipient = null;
        $('#m').on('input', function(e) {
            var text_message = $('#m').val();
            var recipient = getRecipient(text_message);

            if (previous_typing_recipient != recipient) {
                var message = {
                    to   : previous_typing_recipient,
                    from : login,
                    type : "not_typing",
                };
                sendMessage(message);

                previous_typing_recipient = null;
            }
            
            if (previous_typing_recipient == null) {
                var message = {
                    to   : recipient,
                    from : login,
                    type : "typing",
                };
                sendMessage(message);

                previous_typing_recipient = recipient;
            }
        });

        function sendMessage(message) {
            if (message.to && message.from && message.type) {
                socket.emit(public_channel, message);
            }
        }
        /// /메세지 발신
        ///////////////////////////////////////////////////


        ///////////////////////////////////////////////////
        /// 메세지 수신
        socket.on(private_channel, function(msg){
            processIncomingMessage(msg);
        });

        function processIncomingMessage(msg) {
            var sender = msg.from;

            if (msg.type == "text_message") {
                var message = msg.message;
                $('#messages').append($('<li>').text(sender + ": " + message));
            } else if (msg.type == "typing") {
                $('#messages').append($('<li>').addClass(sender + "-typing").text(sender + " is typing"));
            } else if (msg.type == "not_typing") {
                $("li." + sender + "-typing").remove();
            }
        }
        /// 메세지 수신
        ///////////////////////////////////////////////////

        function getRecipient(text_message) {
            recipient = text_message.match(/@\w*\s/) || [];
            return recipient[0] ? recipient[0].trim().substr(1) : null;
        }
    })();
</script>