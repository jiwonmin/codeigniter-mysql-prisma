<div id="content">
    <form action="/login" name="login" method="post">
        <input type="hidden" name="_method" value="patch">

        <div class="ui placeholder segment" style="border:0;background:white;box-shadow:none;">
            <div class="ui two column very relaxed stackable grid">
                <div class="column">
                <div class="ui form">
                    <div class="field">
                    <label>ID</label>
                    <div class="ui left icon input">
                        <input type="text" placeholder="ID" name="id" maxlength="20" required>
                        <i class="user icon"></i>
                    </div>
                    </div>
                    <div class="field">
                    <label>Password</label>
                    <div class="ui left icon input">
                        <input type="password" name="pw" maxlength="250" required>
                        <i class="lock icon"></i>
                    </div>
                    </div>
                    <div class="ui submit button" onclick="loginSubmit(document.login)">Login</div>
                </div>
                </div>
                <div class="middle aligned column">
                <div class="ui big button" onclick="location.href='/join'">
                    <i class="signup icon"></i>
                    Join
                </div>
                </div>
            </div>
            <div class="ui vertical divider">
                Or
            </div>
        </div>
    </form>
</div>