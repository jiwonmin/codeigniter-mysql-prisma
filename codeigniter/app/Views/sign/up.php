<div id="content">
    <form action="/join" method="post" class="ui form">
        <input type="hidden" name="_method" value="post">

        <div class="ui form">
            <div class="field">
                <label>ID</label>
                <div class="ui left icon input">
                    <input type="text" placeholder="ID" name="id" maxlength="20" required>
                    <i class="user icon"></i>
                </div>
            </div>

            <div class="field">
                <label>Name</label>
                <div class="ui left icon input">
                    <input type="text" placeholder="Name" name="name" maxlength="20" required>
                    <i class="tag icon"></i>
                </div>
            </div>

            <div class="field">
                <label>Password</label>
                <div class="ui left icon input">
                    <input type="password" name="pw" maxlength="250" required>
                    <i class="lock icon"></i>
                </div>
            </div>

            <div class="field">
                <label>Retype</label>
                <div class="ui left icon input">
                    <input type="password" name="pw_confirm" maxlength="250" required>
                    <i class="lock icon"></i>
                </div>
            </div>
            <button class="ui button" type="submit">Join</button>
        </div>
    </form>
</div>