<div id="content">
    <table class="ui selectable celled compact table board-list">
        <thead>
            <tr>
                <th>No.</th>
                <th>Subject</th>
                <th>Writer</th>
                <th>CreatedAt</th>
            </tr>
        </thead>
        <tbody v-if="loading" class="ui loading">
            <tr>
                <td colspan="4" style="height:39px;">
                    <div class="ui active centered inline loader"></div>
                </td>
            </tr>
        </tbody>

        <tbody v-if="!loading && lists.length > 0" v-cloak>
            <tr v-for="(list, index) in lists">
                <td>{{ paging.total_no - ((paging.now - 1) * 10) - index }}</td>
                <td class="subject"><a v-bind:href="'/board/' + list.idx">{{ list.title }}</a></td>
                <td>{{ list.user_name }}</td>
                <td>{{ dateFormat(list.created_at) }}</td>
            </tr>
            <tr>
                <td colspan="4">
                    <div class="ui pagination menu">
                        <a class="item" v-on:click="fetchData( paging.first - 1 )" v-if="paging.first > 1"><i class="left chevron icon"></i></a>

                        <a class="item"
                            v-on:click="fetchData( paging.first + i )"
                            v-for="(n,i) in 10"
                            v-if="(paging.first + i) <= paging.last"
                            :class="{ active: (paging.first + i == paging.now) }">{{ paging.first + i }}</a>
                        
                        <a class="item" v-on:click="fetchData( paging.next )" v-if="paging.last != paging.total"><i class="right chevron icon"></i></a>
                    </div>
                </td>
            </tr>
        </tbody>

        <tbody v-if="!loading && lists.length < 1" v-cloak>
            <tr>
                <td colspan="4">작성 된 게시글이 없습니다.</td>
            </tr>
        </tbody>
    </table>

    <div style="display:flex;justify-content:flex-end;">
        <?php if ($_SESSION['IS_LOGIN']) { ?>
            <div class="ui vertical animated button" tabindex="0" onclick="location.href='/board/write'">
                <div class="hidden content">Write</div>
                <div class="visible content">
                    <i class="pencil alternate icon"></i>
                </div>
            </div>
        <?php } ?>
    </div>
</div>


<script>
    $(function() {
        new Vue({
            el: '#content',
            data: {
                lists: [],
                paging: {
                    first:1
                },
                loading: true
            },
            methods: {
                fetchData: function(pageNumber) {

                    axios({
                        method: 'post',
                        url: '/api/boards/page/' + pageNumber
                    }).then(function (response) {
                        this.lists = response.data.lists;
                        this.paging = response.data.paging;

                        this.loading = false;
                    }.bind(this));
                },
                dateFormat(value, event) {
                    var date = new Date(value);
                    var yyyy = date.getFullYear();
                    var mm = String(date.getMonth() + 1).padStart(2, '0');
                    var dd = String(date.getDate()).padStart(2, '0');
                    var h = String(date.getHours()).padStart(2, '0');
                    var i = String(date.getMinutes()).padStart(2, '0');
                    var s = String(date.getSeconds()).padStart(2, '0');

                    return `${yyyy}-${mm}-${dd} ${h}:${i}:${s}`;
                },
            },
            mounted: function() {
                this.fetchData(1);
            }
        });
    });
</script>