<div id="wrap">
    <div id="content">
        <form action="/board/write" method="post" class="ui form">
            <input type="hidden" name="_method" value="post">
            <div class="field">
                <label>Subject</label>
                <input type="text" name="subject" placeholder="Subject" maxlength="250" autocomplete="off" required>
            </div>
            <div class="field">
                <label>Content</label>
                <textarea placeholder="Content" name="content" required></textarea>
            </div>
            <div class="field">
                <label>Writer</label>
                <a class="ui image label">
                    <i class="user icon"></i>
                    <?= $_SESSION['LOGIN_NAME'] ?>
                </a>
            </div>

            <div style="text-align:center;">
                <button class="ui button" type="submit">Write</button>
            </div>
        </form>
    </div>
</div>