<div id="wrap">
    <div id="content">
        <form action="/board" method="post" class="ui form">
            <input type="hidden" name="_method" value="post">
            <input type="hidden" name="idx" value="<?= $data['board']['idx'] ?>">

            <div class="field">
                <label>Subject</label>
                <input type="text" name="subject" placeholder="Subject" value="<?= $data['board']['title'] ?>" maxlength="250" required readonly>
            </div>
            <div class="field">
                <label>Content</label>
                <textarea placeholder="Content" name="content" required readonly><?= $data['board']['contents'] ?></textarea>
            </div>
            <div class="field">
                <label>Writer</label>
                <a class="ui image label">
                    <i class="user icon"></i>
                    <?= $data['board']['user_name'] ?>
                </a>
            </div>
            <div class="field">
                <label>CreatedAt</label>
                <span><?= date('Y-m-d H:i:s', strtotime($data['board']['created_at'])) ?></span>
            </div>

            <? if ($_SESSION['LOGIN_ID'] === $data['board']['user_id']) { ?>
                <div style="text-align:center;">
                    <button id="update" class="ui button" type="button" onclick="updateBoard(this)">Update</button>
                    <button id="delete" class="ui button" type="button" onclick="deleteBoard(this)">Delete</button>
                </div>
            <? } ?>
        </form>
    </div>
</div>