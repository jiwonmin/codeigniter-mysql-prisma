<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// 메인
$routes->get('/', 'Home::index');

// 글목록
$routes->get('/boards', 'Boards::list');                // 기본 글목록
$routes->get('/boards/page/(:num)', 'Boards::list/$1'); // 페이징 글목록

// 글
$routes->get('/board/(:num)', 'Boards::read/$1');               // 읽기
$routes->get('/board/write', 'Boards::write');                  // 생성 폼
$routes->post('/board/write', 'Boards::writeProcess');          // 생성
$routes->put('/board/(:num)', 'Boards::updateProcess/$1');      // 수정
$routes->delete('/board/(:num)', 'Boards::deleteProcess/$1');   // 삭제

// 회원
$routes->get('/join', 'Users::join');            // 가입 폼
$routes->post('/join', 'Users::joinProcess');    // 가입
$routes->get('/login', 'Users::login');          // 로그인 폼
$routes->patch('/login', 'Users::loginProcess'); // 로그인
$routes->get('/logout', 'Users::logout');        // 로그아웃

// API
$routes->post('/api/boards/page/(:num)', 'Api::list/$1');
$routes->patch('/api/user/login', 'Api::login');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
