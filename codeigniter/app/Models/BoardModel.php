<?php


namespace App\Models;

use CodeIgniter\Model;

class BoardModel extends Model
{
    protected $table      = 'boards';
    protected $primaryKey = 'idx';

    // protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    // protected $useSoftDeletes = true;

    protected $allowedFields = ['title', 'contents', 'user_id', 'updated_at'];

    // protected $useTimestamps = false;
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    // protected $skipValidation     = false;

    public function insertBoard(array $board = [])
    {
        $session = session();
        $return = false;
        $data = [
            'title'     => $board['subject'],
            'contents'  => $board['content'],
            'user_id'   => $session->get('LOGIN_ID')
        ];

        try {
            if (!empty($data['title']) && !empty($data['contents']) && !empty($data['user_id'])) {
                $return = $this->insert($data);
            }
        } catch (\Exception $e) {
            // echo $e->getMessage();
        } finally {
            return $return;
        }
    }

    public function updateBoard(string $idx = '', array $board = []):bool
    {
        $session = session();
        $return = false;
        $data = [
            'title'         => $board['subject'],
            'contents'      => $board['content'],
            'user_id'       => $session->get('LOGIN_ID'),
            'updated_at'    => date('Y-m-d H:i:s')
        ];

        try {
            if (!empty($data['title']) && !empty($data['contents']) && !empty($data['user_id'])) {
                $return =
                    $this
                        ->where('user_id', $session->get('LOGIN_ID'))
                        ->where('idx', $idx)
                        ->update($idx, $data);
            }
        } catch (\Exception $e) {
            // echo $e->getMessage();
        } finally {
            return $return;
        }
    }

    public function deleteBoard(string $idx = '')
    {
        $session = session();
        $return = false;

        try {
            if (!empty($idx)) {
                $return =
                    $this
                        ->where('user_id', $session->get('LOGIN_ID'))
                        ->where('idx', $idx)
                        ->delete();
            }
        } catch (\Exception $e) {
            // echo $e->getMessage();
        } finally {
            return $return;
        }
    }

    public function getBoardsList(string $page = '1'):array
    {
        $first_index    = 10 * ($page - 1);
        $list_per_page  = 10;

        $count      = count($this->findAll());
        $total_page = (int) ceil($count / 10);
        $first_page = floor($page / 10 - 0.1) * 10 + 1;
        $last_page  = ceil($page / 10) * 10;
        $last_page  = ($last_page >= $total_page) ? $total_page : $last_page;

        $prev_page  = ($first_page <= 1) ? 1 : $first_page - 1;
        $next_page  = ($last_page < $total_page) ? $last_page + 1 : $total_page;

        $boards['lists'] =
            $this->join('users', 'boards.user_id = users.user_id', 'left')
                ->orderBy('idx', 'desc')
                ->findAll($list_per_page, $first_index);

        $boards['paging'] = array(
            'first'     => $first_page,
            'last'      => $last_page,
            'total'     => $total_page,
            'prev'      => $prev_page,
            'next'      => $next_page,
            'now'       => $page,
            'total_no'  => $count
        );
        return $boards;
    }

    public function selectBoard(string $idx = ''):array
    {
        return $this->join('users', 'boards.user_id = users.user_id', 'left')->find($idx);
    }
}