<?php


namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table      = 'users';
    protected $primaryKey = 'user_id';

    // protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    // protected $useSoftDeletes = true;

    protected $allowedFields = ['user_id', 'password', 'user_name'];

    // protected $useTimestamps = false;
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    // protected $skipValidation     = false;

    public function isExistsUserId(string $userId = ''):bool
    {
        return !empty($this->find($userId));
    }

    public function insertUser(array $user = [])
    {
        $return = false;
        $data = [
            'user_id'   => $user['id'],
            'password'  => $this->password($user['pw']),
            'user_name' => $user['name']
        ];

        try {
            if ($user['pw'] === $user['pw_confirm']) {
                $return = $this->insert($data);
            }
        } catch (\Exception $e) {
            // echo $e->getMessage();
        } finally {
            return $return;
        }
    }

    public function selectUser(array $user = [])
    {
        return $this
            ->where('password', $this->password($user['pw']))
            ->where('user_id', $user['id'])
            ->first();
    }

    function password(string $password):string
    {
        // nodejs: Buffer.from(crypto.createHash('sha512').update(args.password).digest('binary'), 'binary').toString('base64')
        return base64_encode(hash('sha512', $password, true));
    }
}