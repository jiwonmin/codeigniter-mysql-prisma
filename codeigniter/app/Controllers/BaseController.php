<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 */

class BaseController extends Controller
{
	/**
	 * An array of helpers to be loaded automatically upon
	 * class instantiation. These helpers will be available
	 * to all other controllers that extend BaseController.
	 *
	 * @var array
	 */
	protected $helpers = [];

	/**
	 * Constructor.
	 *
	 * @param RequestInterface  $request
	 * @param ResponseInterface $response
	 * @param LoggerInterface   $logger
	 */
	public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
	{
		// Do Not Edit This Line
		parent::initController($request, $response, $logger);

		//--------------------------------------------------------------------
		// Preload any models, libraries, etc, here.
		//--------------------------------------------------------------------
		// E.g.: $this->session = \Config\Services::session();
	}

	public function view($page = 'home', $data = [])
    {
        if (!is_file(APPPATH.'/Views/'.$page.'.php')) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }

        echo view('templates/header', $data);
        echo view($page, $data);
        echo view('templates/footer', $data);
    }

    /**
     * 컨트롤러 로직 처리 중, 이벤트 발생 시 페이지 전환
     *
     *
     * @param string $location  이동할 페이지명
     * @param string $message   이동 전 표시할 메시지
     */
    public function relocation(string $location = '/', string $message = ''):void
    {
        $alert = !empty($message) ? "alert('{$message}');" : "";
        $href  = !empty($location) ? "location.href='{$location}';" : "";
        echo "<script>{$alert}{$href}</script>";
        exit;
    }

    /**
     * $_POST 또는 $_GET 등 입력 값에 대한 기본적인 injection 방어 처리
     *
     *
     * @param array  $request  $_POST, $_GET, $_REQUEST 등의 input 배열 변수
     * @return array 별도 injection 방어 처리 후 동일한 배열 반환
     */
    function injection(array $request = array()):array
    {
        foreach ($request as $key => $req) {
            if (is_array($req)) {
                array_map('injection', $req);
            } else {
                $request[$key] = htmlspecialchars(strip_tags($req), ENT_QUOTES);
            }
        }

        return $request;
    }
}
