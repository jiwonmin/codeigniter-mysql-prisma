<?php

namespace App\Controllers;

use App\Models\BoardModel;

class Boards extends BaseController
{
    public function __construct()
    {
        session();
    }

    public function list(string $pageNumber = '1'):void
    {
        $boardModel = new BoardModel();
        $boards = $boardModel->getBoardsList($pageNumber);

        $data = [
            'data' => [
                'header-title' => 'Boards',
                'lists' => $boards['lists'],
                'paging' => $boards['paging']
            ]
        ];
        $this->view('boards/list', $data);
    }

    public function read(string $idx = ''):void
    {
        $boardModel = new BoardModel();
        $board = $boardModel->selectBoard($idx);

        $data = [
            'data' => [
                'header-title' => 'Board',
                'board' => $board
            ]
        ];
        $this->view('boards/read', $data);
    }

    public function write():void
    {
        $data = [
            'data' => [
                'header-title' => 'Board write'
            ]
        ];
        $this->view('boards/write', $data);
    }

    public function writeProcess():void
    {
        $_POST = $this->injection($_POST);
        $boardModel = new BoardModel();

        $result = $boardModel->insertBoard($_POST);

        if (!is_numeric($result)) {
            $this->relocation('/boards', '게시글 작성에 실패했습니다.');
        } else {
            $this->relocation('/boards', '게시글이 작성되었습니다.');
        }
    }

    public function updateProcess(string $idx = '0'):void
    {
        $_POST = $this->injection($_POST);
        $boardModel = new BoardModel();

        $result = $boardModel->updateBoard($idx, $_POST);
        if (!$result) {
            $this->relocation('/boards', '게시글 수정에 실패했습니다.');
        } else {
            $this->relocation('/boards', '게시글이 수정되었습니다.');
        }
    }

    public function deleteProcess(string $idx = '0')
    {
        $_POST = $this->injection($_POST);
        $boardModel = new BoardModel();

        $result = $boardModel->deleteBoard($idx);
        if (!$result) {
            $this->relocation('/boards', '게시글 삭제에 실패했습니다.');
        } else {
            $this->relocation('/boards', '게시글이 삭제되었습니다.');
        }
    }
}
