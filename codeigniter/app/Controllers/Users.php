<?php

namespace App\Controllers;

use App\Models\UserModel;
use Config\Database;
use PDO;
use PDOException;

class Users extends BaseController
{
    public function join():void
    {
        $data = [
            'data' => [
                'header-title' => 'Join'
            ]
        ];

        $this->view('sign/up', $data);
    }

    public function joinProcess():void
    {
        $_POST = $this->injection($_POST);
        $userModel = new UserModel();

        if ($userModel->isExistsUserId($_POST['id'])) {
            $this->relocation('/join', '이미 존재하는 아이디입니다.');
        } else {
            $result = $userModel->insertUser($_POST);

            if (!is_numeric($result)) {
                $this->relocation('/join', '비밀번호를 동일하게 입력해주세요.');
            } else {
                $this->relocation('/', '가입되었습니다.');
            }
        }
    }

    public function login()
    {
        $data = ['data' => ['header-title' => 'Login']];
        $this->view('sign/in', $data);
    }

    public function loginProcess()
    {
        $session = session();
        $_POST = $this->injection($_POST);
        $userModel = new UserModel();

        if (!$userModel->isExistsUserId($_POST['id'])) {
            $this->relocation('/login', '존재하지 않는 아이디입니다.');
        } else {
            $result = $userModel->selectUser($_POST);
            if (!empty($result)) {
                $sessionData = [
                    'IS_LOGIN'   => true,
                    'LOGIN_ID'   => $result['user_id'],
                    'LOGIN_NAME' => $result['user_name']
                ];
                $session->set($sessionData);

                $this->relocation('/', '로그인되었습니다.');
            } else {
                $this->relocation('/login', '로그인에 실패했습니다.');
            }
        }
    }

    public function logout()
    {
        $session = session();
        $session->destroy();

        $this->relocation('/', '로그아웃되었습니다.');
    }
}
