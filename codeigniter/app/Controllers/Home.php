<?php

namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
	    session();

	    $data = ['data' => ['header-title' => 'Home']];
        $this->view('index', $data);

		// return view('index');
	}
}
