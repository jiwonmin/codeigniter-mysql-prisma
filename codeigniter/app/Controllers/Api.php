<?php

namespace App\Controllers;

use App\Models\BoardModel;
use Exception;

class Api extends BaseController
{
	public function list(string $pageNumber = '1'):string
	{
        $boardModel = new BoardModel();
        $boards = $boardModel->getBoardsList($pageNumber);

	    return json_encode($boards);
	}

	public function login():void
    {
	    $session    = session();

	    $_POST      = $this->injection($_POST);
	    $_method    = $_POST['_method'];
	    $user_id    = $_POST['user_id'];
	    $password   = $_POST['password'];

        $ch         = curl_init();
        $result     = '';
        $message    = '';
        $debug      = '';
        $url        = '';

        try {
            if (!$ch) {
                throw new Exception("curl init error.", 500);
            }

            $request_post_fields = json_encode([
                'query' => trim("
                    query {
                        login (user_id: \"{$user_id}\", password: \"{$password}\") {
                            user_id
                            user_name
                        }
                    }
                ")
            ]);

            curl_setopt($ch, CURLOPT_URL, 'http://host.docker.internal:4000');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request_post_fields);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json;charset=utf-8']);

            $curl_response = curl_exec($ch);

            if (!$curl_response) {
                throw new Exception(sprintf('Curl failed with error #%d: %s.', curl_errno($ch), curl_error($ch)), 500);
            }
            
            $resp = json_decode($curl_response);

            if (isset($resp->errors)) {
                throw new Exception($resp->errors[0]->message, 500);
            }

            if (empty($resp->data->login)) {
                throw new Exception('Empty login info.', 400);
            }

            if (isset($resp->data->login[0]->user_id, $resp->data->login[0]->user_name)) {
                $result = 'success';
                $message = '로그인되었습니다.';
                $url = '/';

                $session->set([
                    'IS_LOGIN'   => true,
                    'LOGIN_ID'   => $resp->data->login[0]->user_id,
                    'LOGIN_NAME' => $resp->data->login[0]->user_name
                ]);
            } else {
                throw new Exception('Server error.', 500);
            }
        } catch (Exception $e) {
            switch ($e->getCode()) {
                case 400:
                case 500:
                default:
                    $result = 'error';
                    $message = '로그인에 실패했습니다.';
                    $debug = $e->getMessage();
                    break;
            }
        } finally {
            curl_close($ch);
            echo json_encode([
                'result' => $result,
                'message' => $message,
                'debug' => $debug,
                'url' => $url
            ]);
        }
    }

    public function password(string $password):string
    {
        return base64_encode(hash('sha512', $password, true));
    }

}
